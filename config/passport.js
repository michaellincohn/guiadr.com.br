var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var bCrypt = require('bcrypt-nodejs');
var Usuario = mongoose.model('Usuario');

module.exports = function(passport) {
    passport.use('local', new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true
        },
        function(req, username, password, done) {
            findUser = function() {
                Usuario.findOne({ 'email' : username }, function(err, usuario) {
                    if(err)
                        return done(err);
                    
                    if (!usuario) {
                        return done(null, false, req.flash('message', 'Usuário não encontrado'));
                    }
                    
                    if (!isValidPassword(usuario, password)) {
                        return done(null, false, req.flash('message', 'Senha inválida'));
                    }
                    
                    return done(null, usuario);
                });
            };

            process.nextTick(findUser);
        })
    );


    passport.use('localAuto', new LocalStrategy({
            passReqToCallback: true
        },
        function(req, user, pass, done) {
            var username = 'michael.lincohn@gmail.com';
            var password = '123';

            findUser = function() {
                Usuario.findOne({ 'email' : username }, function(err, usuario) {
                    if(err)
                        return done(err);
                    
                    if (!usuario) {
                        return done(null, false, req.flash('message', 'Usuário não encontrado'));
                    }
                    
                    if (!isValidPassword(usuario, password)) {
                        return done(null, false, req.flash('message', 'Senha inválida'));
                    }
                    
                    return done(null, usuario);
                });
            };

            process.nextTick(findUser);
        })
    );

    var isValidPassword = function(user, password) {
        return bCrypt.compareSync(password, user.password);
    }

    var createHash = function(password){
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }

    passport.serializeUser(function(user, done) {
        done(null, user._id);
    });

    passport.deserializeUser(function(id, done) {
        Usuario.findById(id, function(err, user) {
            done(err, user);
        });
    });
};