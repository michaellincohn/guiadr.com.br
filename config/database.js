var mongoose = require('mongoose');

module.exports = function () {
    var uri = 'mongodb://localhost:27017/guiadr';
    //var uri = 'mongodb://admin:!q2w3e4r@ds011228.mongolab.com:11228/guiadr';
    var options = {
        server: { poolSize: 5 },
    }

    mongoose.connect(uri, options);
    mongoose.set('debug', true);
    
    mongoose.connection.on('connected', function () { console.log('Mongoose! Conectado em ' + uri); });
    mongoose.connection.on('disconnected', function () { console.log('Mongoose! Desconectado de ' + uri); });
    mongoose.connection.on('error', function (erro) { console.log('Mongoose! Erro na conexão: ' + erro); });
    process.on('SIGINT', function () {
        mongoose.connection.close(function () {
            console.log('Mongoose! Desconectado pelo término da aplicação');
            process.exit(0);
        });
    });
}