var express = require('express');
var load = require('express-load');
var session = require('express-session');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var passport = require('passport');
var helmet = require('helmet');
var flash = require('connect-flash');

module.exports = function () {
    var app = express();

    app.set('port', 3000);

    /*app.use(function(req, res, next) {
        res.setHeader("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");
        res.header("Access-Control-Allow-Origin", "http://localhost:3000");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });*/

    app.set('view engine', 'ejs');
    app.set('views', './app/views');
    app.use(express.static('./public'));

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(require('method-override')());

    app.use(cookieParser());
    app.use(session({
        secret: 'guiadr', 
        resave: true, 
        saveUninitialized: true 
    }));
    app.use(passport.initialize());
    app.use(passport.session());

    app.use(helmet.xframe());
    app.use(helmet.xssFilter());
    app.use(helmet.nosniff());
    app.disable('x-powered-by');

    app.use(flash());

    load('models', { cwd:'app' })
        .then('controllers/painel')
        //.then('controllers/portal')
        .then('routes/painel/auth.js')
        .then('routes/painel')
        //.then('routes/portal')
        .into(app);

    app.get('*', function(req, res) {
        res.status(404).render('404');
    });

    return app;
};