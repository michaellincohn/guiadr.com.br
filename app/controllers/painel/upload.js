var path = require('path');
var lwip = require('lwip');
var fs = require('fs');
var mime = require('mime');

module.exports = function(app) {
    var Model = app.models.usuario;
    var controller = {}

    controller.upload = function(req, res) {
        var file = req.files.file;
        var uploadDir = './public/portal/_ups/temp/';

        fs.readFile(file.path, function (err, data) {
            var uploadPath = uploadDir + file.name;

            fs.writeFile(uploadPath, data, function (err, data) {
                if (err) {
                    console.log(err);
                }

                var dt = new Date();
                var salt = dt.getTime();
                var ext = mime.extension(file.type);
                var name = req.body.nameid + salt + '.' + ext;
                var newname = uploadPath.replace(file.name, name);

                fs.rename(uploadPath, newname, (err) => {
                    if (err) console.log(err);
                });

                res.status(201).json({
                    file: newname.replace('./public/', '')
                });
            });
        });
    };

    controller.resize = function(req, res) {
        var _module = req.params.module;
        var attrs = req.body;

        var file = attrs.file.replace('../', './public/');
        var filecopy = file.replace('/temp/', '/' + _module + '/');

        var coods = attrs.coords;
        var left = coods[0];
        var top = coods[1];
        var right = coods[2];
        var bottom = coods[3];
        var width = attrs.width;
        var height = attrs.height;

        lwip.open(file, function(err, image) {
            if (err) return res.status(201).json({ mensage:'Ocorreu um erro ao editar a imagem!', file:null });
            image.crop(left, top, right, bottom, function (err, image) {
                if (err) return res.status(201).json({ mensage:'Ocorreu um erro ao recortar a imagem!', file:null });
                
                if(height != null) {
                    image.resize(width, height, function (err, image) {
                        if (err) return res.status(201).json({ mensage:'Ocorreu um erro ao redimensionar a imagem!', file:null });
                        image.writeFile(filecopy, function(err) {
                            if (err) return res.status(201).json({ mensage:'Ocorreu um erro ao salvar a imagem!', file:null });
                            fs.unlink(file, (err) => {
                                if (err) return console.log(err);
                            });
                            res.status(201).json({file: filecopy});
                        });
                    });
                } else {
                    image.resize(width, function (err, image) {
                        if (err) return res.status(201).json({ mensage:'Ocorreu um erro ao redimensionar a imagem!', file:null });
                        image.writeFile(filecopy, function(err) {
                            if (err) return res.status(201).json({ mensage:'Ocorreu um erro ao salvar a imagem!', file:null });
                            fs.unlink(file, (err) => {
                                if (err) return console.log(err);
                            });
                            res.status(201).json({ mensagem:'Imagem salvar com sucesso!', file: filecopy});
                        });
                    });
                }
            });
        });
    }

    var randomIntInc = function(low, high) {
        return Math.floor(Math.random() * (high - low + 1) + low);
    }

    return controller;
};