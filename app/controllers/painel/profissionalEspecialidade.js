var sanitize = require('mongo-sanitize');
var bCrypt = require('bcrypt-nodejs');

module.exports = function(app) {
    var Model = app.models.usuario;
    var controller = {}

    controller.getById = function(req, res) {
        var _id = sanitize(req.params.id);
        Model
        .findById(_id)
        .exec()
        .then(function(register) {
            if (!register) throw new Error("Registro não encontrado");
            res.json(register);
        },
        function(erro) {
            res.status(404).json(erro)
        });
    };

    controller.removeById = function(req, res) {
        var _id = sanitize(req.params.id);

        if (_id) {
            Model.update(
                { 'especialidade._id': _id },
                { $pull: { 'especialidade': { '_id': _id } } }
            )
            .exec()
            .then(function (register) {
                res.status(201).json({mensagem:"Registro alterado com sucesso!", class:"btn-info", add:false, id:register._id});
            }, function(erro) {
                res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false, id:0});
            });
        }
    };

    controller.save = function(req, res) {
        var _id = sanitize(req.body._id);

        var habilidade = {};
        var habilidades = [];
        var especialidade = {};
        var especialidades = [];

        habilidades = [];
        for (var i = 0; i < req.body.especialidade.habilidade.length; i++) {
            habilidade = {};
            habilidade['titulo'] = req.body.especialidade.habilidade[i].titulo;
            habilidades.push(habilidade);
        }

        var especialidadeId = req.body.especialidade._id;
        especialidade['titulo'] = req.body.especialidade.titulo;
        especialidade['habilidade'] = habilidades;
        especialidades.push(especialidade);

        if (_id) {
            Model.update(
                { '_id': _id },
                { $pushAll: { 'especialidade': especialidades } }
            )
            .exec()
            .then(function (register) {
                res.status(201).json({mensagem:"Salvar", class:"btn-info", add:false, id:especialidadeId});
            }, function(erro) {
                res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false, id:0});
            });
        }
        else
        {
            res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false, id:0});
        }
    };

    return controller;
};