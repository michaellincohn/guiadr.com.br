var sanitize = require('mongo-sanitize');
var bCrypt = require('bcrypt-nodejs');

module.exports = function(app) {
    var Model = app.models.mensagem;
    var controller = {}
    var perPage = 10;
    var totalItens = 0;

    controller.listAll = function(req, res) {
        var json = {};
        var page = Math.max(0, (req.query.page - 1));
        var conditions = {dtExclusao: null, toId : req.user.id};

        if(req.query.busca) {
            var busca = JSON.parse(req.query.busca);
            for (var i = 0; i < busca.length; i++) {
                conditions[busca[i].key] = getTypeFunction(busca[i].value, busca[i].type);
            }
        }

        Model.find(conditions)
        .limit(perPage)
        .skip(perPage * page)
        .sort({_id: 'asc'})
        .exec()
        .then(function(registers) {
            json.itens = registers;
            if(registers != null) {
                Model.count(conditions, function(e, total) {
                    json.allItens = total;
                    res.json(json);
                });
            }
        },
        function(erro) {
            res.status(500).json(erro);
        });
    };

    controller.getById = function(req, res) {
        var _id = sanitize(req.params.id);
        Model
        .findById(_id)
        .exec()
        .then(function(register) {
            if (!register) throw new Error("Registro não encontrado");
            res.json(register);
        },
        function(erro) {
            res.status(404).json(erro)
        });
    };

    controller.removeById = function(req, res) {
        var _id = sanitize(req.params.id);

        if (_id) {
            var dados = {
               "dtExclusao": Date.now()
            }

            Model
            .findByIdAndUpdate(_id, dados)
            .exec()
            .then(function(reg) {
                res.end();
            }, function(erro) {
                console.log(erro);
            });
        }
    };

    controller.example = function(req, res) {
        var newUser = new Model({
            "nome": 'Michael Lincohn',
            "email": 'michael.lincohn@gmail.com',
            "message": 'Lorem ipsum dolor sit amet, dicant nonumy aliquam et ius, dictas mollis forensibus no eos, quo ad novum tantas. Oratio feugait epicuri ea sed, duo et erant dolores. Alterum legendos atomorum has ex, at diam soleat latine mea. Movet altera nam ea. Ne prompta voluptaria vim, ad viris convenire reformidans pro.',
            "toId": req.user.id,
        });
        newUser.save(function(err) {
            if(err) {
                console.log('Erro ao salvar usuário: ' + err);
                throw err;
            }
        });

        newUser = new Model({
            "nome": 'Iris Regina',
            "email": 'iris_libras@hotmail.com',
            "message": 'Lorem ipsum dolor sit amet, dicant nonumy aliquam et ius, dictas mollis forensibus no eos, quo ad novum tantas. Oratio feugait epicuri ea sed, duo et erant dolores. Alterum legendos atomorum has ex, at diam soleat latine mea. Movet altera nam ea. Ne prompta voluptaria vim, ad viris convenire reformidans pro.',
            "toId": req.user.id,
        });
        newUser.save(function(err) {
            if(err) {
                console.log('Erro ao salvar usuário: ' + err);
                throw err;
            }
        });

        newUser = new Model({
            "nome": 'Ari Melo',
            "email": 'ari.melo@hotmail.com',
            "message": 'Lorem ipsum dolor sit amet, dicant nonumy aliquam et ius, dictas mollis forensibus no eos, quo ad novum tantas. Oratio feugait epicuri ea sed, duo et erant dolores. Alterum legendos atomorum has ex, at diam soleat latine mea. Movet altera nam ea. Ne prompta voluptaria vim, ad viris convenire reformidans pro.',
            "toId": req.user.id,
        });
        newUser.save(function(err) {
            if(err) {
                console.log('Erro ao salvar usuário: ' + err);
                throw err;
            }
        });

        newUser = new Model({
            "nome": 'Maria Madalena',
            "email": 'maria.madalena@hotmail.com',
            "message": 'Lorem ipsum dolor sit amet, dicant nonumy aliquam et ius, dictas mollis forensibus no eos, quo ad novum tantas. Oratio feugait epicuri ea sed, duo et erant dolores. Alterum legendos atomorum has ex, at diam soleat latine mea. Movet altera nam ea. Ne prompta voluptaria vim, ad viris convenire reformidans pro.',
            "toId": req.user.id,
        });
        newUser.save(function(err) {
            if(err) {
                console.log('Erro ao salvar usuário: ' + err);
                throw err;
            }
        });

        newUser = new Model({
            "nome": 'Joseana Camila',
            "email": 'joseana.camila@hotmail.com',
            "message": 'Lorem ipsum dolor sit amet, dicant nonumy aliquam et ius, dictas mollis forensibus no eos, quo ad novum tantas. Oratio feugait epicuri ea sed, duo et erant dolores. Alterum legendos atomorum has ex, at diam soleat latine mea. Movet altera nam ea. Ne prompta voluptaria vim, ad viris convenire reformidans pro.',
            "toId": req.user.id,
        });
        newUser.save(function(err) {
            if(err) {
                console.log('Erro ao salvar usuário: ' + err);
                throw err;
            }
        });

        newUser = new Model({
            "nome": 'Josimary Karina',
            "email": 'josimary.karina@hotmail.com',
            "message": 'Lorem ipsum dolor sit amet, dicant nonumy aliquam et ius, dictas mollis forensibus no eos, quo ad novum tantas. Oratio feugait epicuri ea sed, duo et erant dolores. Alterum legendos atomorum has ex, at diam soleat latine mea. Movet altera nam ea. Ne prompta voluptaria vim, ad viris convenire reformidans pro.',
            "toId": req.user.id,
        });
        newUser.save(function(err) {
            if(err) {
                console.log('Erro ao salvar usuário: ' + err);
                throw err;
            }
        });

        newUser = new Model({
            "nome": 'Jocielly Kricia',
            "email": 'jocielly.kricia@hotmail.com',
            "message": 'Lorem ipsum dolor sit amet, dicant nonumy aliquam et ius, dictas mollis forensibus no eos, quo ad novum tantas. Oratio feugait epicuri ea sed, duo et erant dolores. Alterum legendos atomorum has ex, at diam soleat latine mea. Movet altera nam ea. Ne prompta voluptaria vim, ad viris convenire reformidans pro.',
            "toId": req.user.id,
        });
        newUser.save(function(err) {
            if(err) {
                console.log('Erro ao salvar usuário: ' + err);
                throw err;
            }
        });

        newUser = new Model({
            "nome": 'João Heber',
            "email": 'joao.heber@hotmail.com',
            "message": 'Lorem ipsum dolor sit amet, dicant nonumy aliquam et ius, dictas mollis forensibus no eos, quo ad novum tantas. Oratio feugait epicuri ea sed, duo et erant dolores. Alterum legendos atomorum has ex, at diam soleat latine mea. Movet altera nam ea. Ne prompta voluptaria vim, ad viris convenire reformidans pro.',
            "toId": req.user.id,
        });
        newUser.save(function(err) {
            if(err) {
                console.log('Erro ao salvar usuário: ' + err);
                throw err;
            }
        });

        newUser = new Model({
            "nome": 'Orlando Gonçalves',
            "email": 'orlando.goncalves@hotmail.com',
            "message": 'Lorem ipsum dolor sit amet, dicant nonumy aliquam et ius, dictas mollis forensibus no eos, quo ad novum tantas. Oratio feugait epicuri ea sed, duo et erant dolores. Alterum legendos atomorum has ex, at diam soleat latine mea. Movet altera nam ea. Ne prompta voluptaria vim, ad viris convenire reformidans pro.',
            "toId": req.user.id,
        });
        newUser.save(function(err) {
            if(err) {
                console.log('Erro ao salvar usuário: ' + err);
                throw err;
            }
        });

        newUser = new Model({
            "nome": 'Maria Ivanilda',
            "email": 'maria.ivanilda@hotmail.com',
            "message": 'Lorem ipsum dolor sit amet, dicant nonumy aliquam et ius, dictas mollis forensibus no eos, quo ad novum tantas. Oratio feugait epicuri ea sed, duo et erant dolores. Alterum legendos atomorum has ex, at diam soleat latine mea. Movet altera nam ea. Ne prompta voluptaria vim, ad viris convenire reformidans pro.',
            "toId": req.user.id,
        });
        newUser.save(function(err) {
            if(err) {
                console.log('Erro ao salvar usuário: ' + err);
                throw err;
            }
        });
    };

    var getTypeFunction = function(value, type) {
        if(type == 'like')
            return new RegExp(value, 'i');
        else if(type == 'equals')
            return {$e : value};
        else if(type == 'notequals')
            return {$ne : value};
    }

    return controller;
};