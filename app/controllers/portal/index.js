var sanitize = require('mongo-sanitize');

module.exports = function(app) {
    var ProjetoModel = app.models.projeto;
    var controller = {}

    controller.listAll = function(req, res) {
        res.status(200);
    };

    controller.getById = function(req, res) {
        var _id = sanitize(req.params.id);
        ProjetoModel
        .findById(_id)
        .exec()
        .then(function(register) {
            if (!register) throw new Error("Registro não encontrado");
            res.json(register);
        },
        function(erro) {
            res.status(404).json(erro)
        });
    };

    controller.removeById = function(req, res) {
        var _id = sanitize(req.params.id);
        if (_id) {
            var dados = {
               "idExclusao": req.user.id|| null,
               "dtExclusao": new Date().getTime() || null
            }
            
            ProjetoModel
            .findByIdAndUpdate(_id, dados)
            .exec()
            .then(function() {
                res.end();
            },
            function(erro) {
                return console.error(erro);
            });
        }
    };

    controller.save = function(req, res) {
        var _id = req.body._id;
        var dados = {
            "titulo": req.body.titulo,
            "tipo": req.body.tipo,
            "responsavel": req.user.id,
            "prioridade": req.body.prioridade,
            "situacao": req.body.situacao,
            "descricao": req.body.descricao || null,
            "dataInicio": req.body.dataInicio || null,
            "dataPrevisto": req.body.dataPrevisto || null,
            "dataConcluido": req.body.dataConcluido || null
        };

        if (_id) {
            ProjetoModel
            .findByIdAndUpdate(_id, dados)
            .exec()
            .then(function(register) {
                res.json(register);
            },
            function(erro) {
                res.status(500).json(erro);
            });
        } else {
            for(var i=1; i<200; i++)
            {
                dados['idInclusao'] = req.user.id || null;
                dados['dtInclusao'] = new Date().getTime() || null;
                dados['idExclusao'] = null;
                dados['dtExclusao'] = null;

                ProjetoModel
                .create(dados)
                .then(function(register) {
                    res.status(201).json(register);
                },
                function(erro) {
                    res.status(500).json(erro);
                });
            }
        }
    };

    return controller;
};