var sanitize = require('mongo-sanitize');

module.exports = function(app) {
    var Model = app.models.banner;
    var controller = {}

    controller.list = function(req, res) {
        //save();

        Model
        .find({isAtivo: true})
        .sort({_id: 'desc'})
        .exec()
        .then(function(itens) {
            res.json(itens);
        },
        function(erro) {
            res.status(500).json(erro);
        });
    };


    function save() {
        var dados = {
            "titulo": 'Agende agora sua consulta',
            "chamada": 'A forma mais fácil e rápida de agendar sua consulta com os melhores profissionais do Estado!',
            "slug": 'servicos',
            "image": 'http://placehold.it/1280x960',
            "isAtivo": true,
            "dtInicio": new Date().getTime(),
            "dtFim": new Date().getTime() + 1
        };

        Model.create(dados).then(function(register) {
            res.status(201).json(register);
        },
        function(erro) {
            res.status(500).json(erro);
        });
    }

    return controller;
};