var sanitize = require('mongo-sanitize');

module.exports = function(app) {
    var Model = app.models.profissionais;
    var controller = {}
    var perPage = 10;
    var totalItens = 0;

    controller.listAll = function(req, res) {
        var json = {};
        var page = Math.max(0, (req.query.page - 1));

        Model
        .find({idExclusao: null})
        .populate('tipo')
        .populate('prioridade')
        .populate('situacao')
        .populate('responsavel')
        .limit(perPage)
        .skip(perPage * page)
        .sort({_id: 'asc'})
        .exec()
        .then(function(registers) {
            json.itens = registers;
            Model.find({idExclusao: null}, function(err, itens) {
                json.allItens = itens.length;
                res.json(json);
            });
        },
        function(erro) {
            res.status(500).json(erro);
        });
    };

    controller.getById = function(req, res) {
        var _id = sanitize(req.params.id);
        Model
        .findById(_id)
        .exec()
        .then(function(register) {
            if (!register) throw new Error("Registro não encontrado");
            res.json(register);
        },
        function(erro) {
            res.status(404).json(erro)
        });
    };



    controller.getBySpeciality = function(req, res) {
        var _id = sanitize(req.params.id);
        Model
        .find({speciality:_id})
        .exec()
        .then(function(register) {
            if (!register) throw new Error("Registro não encontrado");
            res.json(register);
        },
        function(erro) {
            res.status(404).json(erro)
        });
    };

    return controller;
};