function verificaAutenticacao(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        res.status('401').json('Não autorizado');
    }
}

module.exports = function(app) {
    var ctrl = app.controllers.painel.clinica;

    app.route('/painel/clinica')
        .get(verificaAutenticacao, ctrl.listAll)
        .post(verificaAutenticacao, ctrl.save);

    app.route('/painel/clinica/:id')
        .get(verificaAutenticacao, ctrl.getById)
        .delete(verificaAutenticacao, ctrl.removeById);
};