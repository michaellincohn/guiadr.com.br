var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

function verificaAutenticacao(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        res.status('401').json('Não autorizado');
    }
}

module.exports = function(app) {
    var ctrl = app.controllers.painel.upload;

    app.route('/painel/upload/:module')
        .post(verificaAutenticacao, multipartMiddleware, ctrl.upload);

    app.route('/painel/resize/:module')
        .post(verificaAutenticacao, ctrl.resize);
};