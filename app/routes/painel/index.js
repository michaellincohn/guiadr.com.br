function verificaAutenticacao(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        res.redirect('/painel/auth/logout');
    }
}

module.exports = function(app) {
    var ctrl = app.controllers.painel.index;
    app.route('/painel').get(verificaAutenticacao, ctrl.home);
};