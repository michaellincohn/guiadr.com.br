function verificaAutenticacao(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        res.status('401').json('Não autorizado');
    }
}

module.exports = function(app) {
    var ctrl = app.controllers.painel.pagina;

    app.route('/painel/pagina')
        .get(verificaAutenticacao, ctrl.listAll)
        .post(verificaAutenticacao, ctrl.save);

    app.route('/painel/pagina/:id')
        .get(verificaAutenticacao, ctrl.getById)
        .delete(verificaAutenticacao, ctrl.removeById);
};