function verificaAutenticacao(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        res.status('401').json('Não autorizado');
    }
}

module.exports = function(app) {
    var ctrl = app.controllers.painel.profissionalLocais;

    app.route('/painel/profissionalLocais')
        .get(verificaAutenticacao, ctrl.getById)
        .post(verificaAutenticacao, ctrl.save);

    app.route('/painel/profissionalLocais/:id')
        .get(verificaAutenticacao, ctrl.getById)
        .delete(verificaAutenticacao, ctrl.removeById);
};