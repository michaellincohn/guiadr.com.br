function verificaAutenticacao(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        res.status('401').json('Não autorizado');
    }
}

module.exports = function(app) {
    var ctrl = app.controllers.painel.especialidade;

    app.route('/painel/especialidade/insert').get(ctrl.example);

    app.route('/painel/especialidade')
        .get(verificaAutenticacao, ctrl.listAll)
        .post(verificaAutenticacao, ctrl.save);

    app.route('/painel/especialidade/:id')
        .get(verificaAutenticacao, ctrl.getById)
        .delete(verificaAutenticacao, ctrl.removeById);
};