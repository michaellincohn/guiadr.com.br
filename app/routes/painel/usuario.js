function verificaAutenticacao(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        res.status('401').json('Não autorizado');
    }
}

module.exports = function(app) {
    var ctrl = app.controllers.painel.usuario;

    app.route('/painel/usuario/insert').get(ctrl.example);

    app.route('/painel/usuario')
        .get(verificaAutenticacao, ctrl.listAll)
        .post(verificaAutenticacao, ctrl.save);

    app.route('/painel/usuario/:id')
        .get(verificaAutenticacao, ctrl.getById)
        .delete(verificaAutenticacao, ctrl.removeById);
};