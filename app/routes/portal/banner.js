module.exports = function(app) {
    var controller = app.controllers.portal.banner;
    app.route('/banner').get(controller.list);
};