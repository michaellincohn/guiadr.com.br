function verificaAutenticacao(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        res.status('401').json('Não autorizado');
    }
}

module.exports = function(app) {
    var controller = app.controllers.projeto;

    app.route('/projeto')
        .get(verificaAutenticacao, controller.listAll)
        .post(verificaAutenticacao, controller.save)

    app.route('/projeto/:id')
        .get(verificaAutenticacao, controller.getById)
        .delete(verificaAutenticacao, controller.removeById);
};