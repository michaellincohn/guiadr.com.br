module.exports = function(app) {
    var controller = app.controllers.profissionais;

    app.route('/profissionais').get(controller.listAll);
    app.route('/profissionais/:id').get(controller.getById);
    app.route('/profissionais/especialidade/:id').get(controller.getBySpeciality);
};