var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

/*
    Os tipo de usuários:
    1 = Administrador
    2 = Profissional
    3 = Clinica
*/

module.exports = function () {
    var habilidadeSchema = mongoose.Schema({ titulo: String });
    var especialidadeSchema = mongoose.Schema({ titulo: String, habilidade: [habilidadeSchema] });
    var redesociaisSchema = mongoose.Schema({ linkedin: String, facebook: String, twitter: String });
    var infoSchema = mongoose.Schema({ descricao: String, educacao: String, numdoc: String });
    var telefoneSchema = mongoose.Schema({ numero: String });
    var enderecoSchema = mongoose.Schema({ logradouro: String, numero: String, bairro: String, cidade: String, pais: String, latlong: String, telefone: [telefoneSchema] });

    var schema = mongoose.Schema({
        nome: String,
        email: String,
        password: String,
        imagem: String,
        tipo: Number,
        redesociais: redesociaisSchema,
        info: infoSchema,
        especialidade: [especialidadeSchema],
        endereco: [enderecoSchema],
        referencia: [{  // Ex: profissional relacionado a uma clinica
            type: mongoose.Schema.ObjectId,
            ref: 'Usuario'
        }],
        dtInclusao: {
            type: Date,
            default: Date.now()
        },
        dtExclusao: {
            type: Date,
            default: null
        }
    }, {
        versionKey: false
    });

    return mongoose.model('Usuario', schema);
};