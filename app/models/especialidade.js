var mongoose = require('mongoose');

module.exports = function () {
    var habilidadeSchema = mongoose.Schema({ titulo: String });
    var schema = mongoose.Schema({
        titulo: String,
        habilidade: [habilidadeSchema],
        dtInclusao: {
            type: Date,
            default: Date.now()
        },
        dtExclusao: {
            type: Date,
            default: null
        }
    });

    return mongoose.model('Especialidade', schema);
};