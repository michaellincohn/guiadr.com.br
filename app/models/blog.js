var mongoose = require('mongoose');

module.exports = function () {
    var categoriaSchema = mongoose.Schema({ titulo: String });
    var comentarioSchema = mongoose.Schema({ titulo: String, descricao: String, usuario: String, dtInclusao: { type: Date, default: Date.now() } });

    var schema = mongoose.Schema({
        titulo: String,
        categoria: categoriaSchema,
        chamada: String,
        conteudo: String,
        imagem: String,
        visualizacoes: { type: Number, default: 0 },
        curtidas: { type: Number, default: 0 },
        comentarios: [comentarioSchema],
        dtPublicacao: {
            type: Date,
            default: Date.now()
        },
        dtInclusao: {
            type: Date,
            default: Date.now()
        },
        dtExclusao: {
            type: Date,
            default: null
        }
    }, {
        versionKey: false
    });

    return mongoose.model('Blog', schema);
};