var http = require('http');
var passport = require('passport');
var app  = require('./config/express')();
require('./config/passport')(passport);
require('./config/database.js')();

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express Server escutando na porta ' + app.get('port'));
});