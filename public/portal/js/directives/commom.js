(function () {
    var stickers = function () {
        return {
            restrict: 'E',
            templateUrl: 'portal/partials/directives/sticker.html',
            controller: function(Api, $scope, $element, $attrs) {

                $scope.profissionais = function() {
                    Api.ProfissionaisEspecialidade.get({id:$scope.especialidade},
                        function (registros) {
                            //$elem.find("select#profissionais");
                        },
                        function (erro) {
                            //$elem.find("select#profissionais");
                        }
                    );
                }
            },
            link: function(scope, element, attrs, Api) {
                element.find("select.custom").ikSelect({ ddFullWidth: false });
                element.find("#book_an_appointment_date").datepicker({ showOtherMonths: true});
                $(document).find("#ui-datepicker-div").addClass("ll-skin-melon");

                //angular.element(this).change(this.profissionais);

                /*$elem.find("select#especialidade").on('change', function (e) {
                    console.log(this.value);
                    Api.Profissionais.get({especialidade_id:e.val()},
                        function (registros) {
                            //$elem.find("select#profissionais");
                        },
                        function (erro) {
                            //$elem.find("select#profissionais");
                        }
                    );
                });*/
            }
        };
    };

    var wrapOwlcarousel = function () {
        return { 
            restrict: 'E', 
            templateUrl: 'portal/partials/directives/banner.html',
            link: function (scope, element, attrs) { 
                var options = scope.$eval($(element).attr('data-options')); 
                $(element).owlCarousel(options);
            } 
        };
    };

    var datepicker = function () {
        return {
            restrict: 'A',
            require : 'ngModel',
            link : function (scope, element, attrs, ngModelCtrl) {
                $(function(){
                    element.datepicker({
                        dateFormat:'dd/mm/yyyy',
                        language: 'pt-BR',
                        autoclose: true,
                        onSelect:function (date) {
                            scope.$apply(function () {
                                ngModelCtrl.$setViewValue(date);
                            });
                        }
                    });
                });
            }
        };
    };

    var dateFormat = function () {
        return {
            restrict: 'A',
            link: function(scope, el, at) {
                console.log(el);
                if(el.val() != "") {
                    var format = at.dateFormat;
                    scope.$watch(at.ngModel, function(date) {
                        var result = moment(date).format(format);
                        el.val(result);
                    });
                }
            }
        };
    };

    var appDatetime = function ($window) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                var moment = $window.moment;

                ngModel.$formatters.push(formatter);
                ngModel.$parsers.push(parser);

                element.on('change', function (e) {
                    var element = e.target;
                    element.value = formatter(ngModel.$modelValue);
                });

                function parser(value) {
                    var m = moment(value);
                    var valid = m.isValid();
                    ngModel.$setValidity('datetime', valid);
                    if (valid) return m.valueOf();
                    else return value;
                }

                function formatter(value) {
                    var m = moment(value);
                    var valid = m.isValid();
                    if (valid) return m.format("LLLL");
                    else return value;
                }
            }
        };
    };

    webApp
    .directive('stickers', stickers)
    .directive('wrapOwlcarousel', wrapOwlcarousel)
    .directive('datepicker', datepicker)
    .directive('dateFormat', dateFormat)
    .directive('appDatetime', appDatetime);
}());