angular.module('guiadr')
.factory('Api', function($resource) {
	return {
        ProfissionaisEspecialidade: $resource('/profissionais/especialidade/:id', {id: '@id'}),
		Banner: $resource('/banner/:id', {id: '@id'}),
        Usuario: $resource('/usuario/:id', {id: '@id'})
	}
})

// Commom Services
.factory('Auth', function($http, $q) {
	var r = $q.defer();
	$http.get('/auth/data').then(function(user) {
		r.resolve(user.data);
	});
	return r.promise;
})
.factory('meuInterceptor', function($q, $location) {  
    var meuInterceptor = {
    	responseError: function(resp) {
    		if (resp.status == 401) {
    		  $location.path('/auth/logout');
    		}
            return $q.reject(resp);
    	}
    }

    return meuInterceptor;
});