var webApp = angular.module('guiadr',['ngRoute', 'ngResource'/*, 'ui.bootstrap'*/]);

webApp.config(function($locationProvider, $routeProvider, ngJcropConfigProvider) {
    //$locationProvider.html5Mode(true);
    //$locationProvider.hashPrefix('!');

    $routeProvider.when('/home', { templateUrl: 'portal/partials/single/index.html', controller: 'IndexCtrl' });
    $routeProvider.when('/sobre', { templateUrl: 'portal/partials/single/sobre.html' });
    $routeProvider.when('/servicos', { templateUrl: 'portal/partials/single/servicos.html' });
    $routeProvider.when('/blog', { templateUrl: 'portal/partials/blog/lista.html' });
    $routeProvider.when('/contato', { templateUrl: 'portal/partials/single/contato.html' });
    
    //$routeProvider.when('/profissional/segmento/:registerId', { templateUrl: 'partials/profissionais/segmento/list.html', controller: 'SegmentoController' });
    //$routeProvider.when('/profissional/:registerId', { templateUrl: 'partials/profissionais/list.html', controller: 'ProfissionalController' });

    $routeProvider.otherwise({ redirectTo: '/home' });
});


app.config(function(ngJcropConfigProvider){

    // [optional] To change the jcrop configuration
    // All jcrop settings are in: http://deepliquid.com/content/Jcrop_Manual.html#Setting_Options
    ngJcropConfigProvider.setJcropConfig({
        bgColor: 'black',
        bgOpacity: .4,
        aspectRatio: 16 / 9
    });

    // [optional] To change the css style in the preview image
    ngJcropConfigProvider.setPreviewStyle({
        'width': '100px',
        'height': '100px',
        'overflow': 'hidden',
        'margin-left': '5px'
    });

});