angular.module('guiadr').controller('UsuarioController', function (Api, Auth, $scope, $routeParams, $location, $timeout) {
    $scope.registros = [];
    $scope.filtro = '';
    $scope.mensagem = { texto: '' };
    $scope.nomePagina = 'Lista';
    $scope.pageSize = 10;
    $scope.currentPage = 1;
    $scope.maxSize = 10;
    $scope.pageChanged = function() {
        listAll();
    };

    if ($routeParams.registerId) {
        $scope.nomePagina = 'Edição';
        getById();
    } else {
        listAll();
    }
    
    function listAll() {
        Api.Usuario.get({page:$scope.currentPage},
            function (registros) {
                $scope.registros = registros.itens;
                $scope.totalItems = registros.allItens;
                $scope.mensagem = {};
            },
            function (erro) {
                $scope.mensagem = { texto: 'Erro: ' + JSON.stringify(erro) };
            }
        );
    }
    
    function getById() {
        Api.Usuario.get(
            { id: $routeParams.registerId },
            function (registro) {
                $scope.registro = registro;
            }, 
            function (erro) {
                $scope.mensagem = { texto: 'Registro inexistente!'};
            }
        );
    }
    
    $scope.remove = function (registro) {
        bootbox.confirm("Confirma a exclusão desse registro?", function(answer) {
            if(answer == true) {
                Api.Usuario.delete(
                    { id: registro._id }, 
                    listAll, 
                    function (erro) {
                        bootbox.alert('Error: ' + erro);
                    }
                );
            }
        });
    };
    
    $scope.salva = function () {
        $scope.registro.$save()
        .then(function () {
            $('#botaoSalvar').text('Registro salvo com sucesso!').removeClass('btn-primary').addClass('btn-success');
            $scope.registro = new Api.Usuario();
        })
		.catch(function (erro) {
            console.log(erro);
        });
    };
});