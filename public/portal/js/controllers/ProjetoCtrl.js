angular.module('guiadr').controller('ProjetoController', function (Api, Auth, $scope, $routeParams, $location, $timeout) {
    $scope.registros = [];
    $scope.filtro = '';
    $scope.mensagem = { texto: '' };
    $scope.nomePagina = 'Lista';
    $scope.pageSize = 10;
    $scope.currentPage = 1;
    $scope.maxSize = 10;
    $scope.pageChanged = function() {
        listAll();
    };

    if ($routeParams.registerId) {
        Api.Prioridade.get(function(registros) {
            $scope.prioridades = registros.itens;
        });

        Api.Tipo.get(function(registros) {
            $scope.tipos = registros.itens;
        });    

        Api.Situacao.get(function(registros) {
            $scope.situacoes = registros.itens;
        });

        if ($routeParams.registerId == "add") {
            $scope.registro = new Api.Projeto();
            $scope.nomePagina = 'Cadastro';

            Auth.then(function(user) {
                $scope.username = user.nome;
            });
        } else {
            $scope.nomePagina = 'Edição';
            getById();
        }
    } else {
        listAll();
    }
    
    function listAll() {
        Api.Projeto.get({page:$scope.currentPage},
            function (registros) {
                $scope.registros = registros.itens;
                $scope.totalItems = registros.allItens;
                $scope.mensagem = {};
            },
            function (erro) {
                $scope.mensagem = { texto: 'Erro: ' + JSON.stringify(erro) };
            }
        );
    }
    
    function getById() {
        Api.Projeto.get(
            { id: $routeParams.registerId },
            function (registro) {
                $scope.registro = registro;
                $scope.username = JSON.stringify(registro.responsavel.nome);
            }, 
            function (erro) {
                $scope.mensagem = { texto: 'Registro inexistente!'};
            }
        );
    }
    
    $scope.remove = function (registro) {
        bootbox.confirm("Confirma a exclusão desse registro?", function(answer) {
            if(answer == true) {
                Api.Projeto.delete(
                    { id: registro._id }, 
                    listAll, 
                    function (erro) {
                        bootbox.alert('Error: ' + erro);
                    }
                );
            }
        });
    };
    
    $scope.salva = function () {
        $scope.registro.$save()
        .then(function () {
            $scope.mensagem = { texto: 'Registro salvo com sucesso!' };
            $scope.registro = new Api.Projeto();
        })
		.catch(function (erro) {
            //bootbox.alert('Error: ' + erro);
        });
    };
});