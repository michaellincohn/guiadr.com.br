angular.module('guiadr', ['ngRoute', 'ngResource', 'ngAnimate', 'ngSanitize', 'ui.bootstrap', 'angular-loading-bar', 'ngTagsInput', 'ngFileUpload', 'ngJcrop', 'ngCkeditor'])
.config(['$routeProvider', '$httpProvider', 'cfpLoadingBarProvider', 'ngJcropConfigProvider', function($routeProvider, $httpProvider, cfpLoadingBarProvider, ngJcropConfigProvider) {
   
    $routeProvider.when('/pagina', { templateUrl: 'partials/pagina/list.html', controller: 'PaginaPainelCtrl' });
    $routeProvider.when('/pagina/:registerId', { templateUrl: 'partials/pagina/form.html', controller: 'PaginaPainelCtrl' });

    $routeProvider.when('/profissional', { templateUrl: 'partials/profissional/list.html', controller: 'ProfissionalPainelCtrl' });
    $routeProvider.when('/profissional/:registerId', { templateUrl: 'partials/profissional/form.html', controller: 'ProfissionalPainelCtrl' });
    $routeProvider.when('/profissionalEspecialidade/:registerId', { templateUrl: 'partials/profissional/formEspecialidade.html', controller: 'ProfissionalEspecialidadePainelCtrl' });
    $routeProvider.when('/profissionalLocais/:registerId', { templateUrl: 'partials/profissional/formLocais.html', controller: 'ProfissionalLocaisPainelCtrl' });
    $routeProvider.when('/profissionalPerfil/:registerId', { templateUrl: 'partials/profissional/formPerfil.html', controller: 'ProfissionalPerfilPainelCtrl' });

    $routeProvider.when('/clinica', { templateUrl: 'partials/clinica/list.html', controller: 'ClinicaPainelCtrl' });
    $routeProvider.when('/clinica/:registerId', { templateUrl: 'partials/clinica/form.html', controller: 'ClinicaPainelCtrl' });
    $routeProvider.when('/clinicaProfissional/:registerId', { templateUrl: 'partials/clinica/formProfissionais.html', controller: 'ClinicaProfissionalPainelCtrl' });
    $routeProvider.when('/clinicaLocais/:registerId', { templateUrl: 'partials/clinica/formLocais.html', controller: 'ClinicaLocaisPainelCtrl' });
    $routeProvider.when('/clinicaPerfil/:registerId', { templateUrl: 'partials/clinica/formPerfil.html', controller: 'ClinicaPerfilPainelCtrl' });

    $routeProvider.when('/especialidade', { templateUrl: 'partials/especialidade/list.html', controller: 'EspecialidadePainelCtrl' });
    $routeProvider.when('/especialidade/:registerId', { templateUrl: 'partials/especialidade/form.html', controller: 'EspecialidadePainelCtrl' });

    $routeProvider.when('/faq', { templateUrl: 'partials/faq/list.html', controller: 'FaqPainelCtrl' });
    $routeProvider.when('/faq/:registerId', { templateUrl: 'partials/faq/form.html', controller: 'FaqPainelCtrl' });

    $routeProvider.when('/mensagem', { templateUrl: 'partials/mensagem/list.html', controller: 'MensagemPainelCtrl' });
    $routeProvider.when('/mensagem/:registerId', { templateUrl: 'partials/mensagem/form.html', controller: 'MensagemPainelCtrl' });

    $routeProvider.when('/usuario', { templateUrl: 'partials/usuario/list.html', controller: 'UsuarioPainelCtrl' });
    $routeProvider.when('/usuario/:registerId', { templateUrl: 'partials/usuario/form.html', controller: 'UsuarioPainelCtrl' });

    $routeProvider.when('/conta', { templateUrl: 'partials/usuario/formConta.html', controller: 'UsuarioContaPainelCtrl' });
    $routeProvider.when('/home', { templateUrl: 'partials/index.html', controller: 'IndexPainelCtrl' });
    $routeProvider.otherwise({ redirectTo: '/home' });

    $httpProvider.interceptors.push('Interceptor');

    // https://github.com/chieffancypants/angular-loading-bar
    cfpLoadingBarProvider.includeSpinner = true;
    cfpLoadingBarProvider.includeBar = true;

    ngJcropConfigProvider.setJcropConfig({
        bgColor: 'black',
        bgOpacity: .3,
        //aspectRatio: 9 / 6
    });

    ngJcropConfigProvider.setPreviewStyle({
        'width': '205px',
        'height': '205px',
        'overflow': 'hidden',
        'margin-left': '5px'
    });
}]);