(function () {
    var toShortDateBrazil = function () {
        return function(value) {
            /*
                LT : 'HH:mm',
                L : 'DD/MM/YYYY',
                LL : 'D [de] MMMM [de] YYYY',
                LLL : 'D [de] MMMM [de] YYYY [às] LT',
                LLLL : 'dddd, D [de] MMMM [de] YYYY [às] LT'
                https://gist.github.com/fernandosavio/680a2549e417befea930
            */
            return moment(value).format("L");
        };
    };

    var toStrFromObj = function () {
        return function(obj) {
            var habilidades = '';
            for (var i = 0; i < obj.length; i++) {
                habilidades += obj[i].titulo + ', ';
            }
            habilidades = habilidades.substring(0, (habilidades.length - 2))
            return habilidades;
        }
    };

    var totalObj = function () {
        return function(obj) {
            return (obj != null ? parseInt(obj.length) : 0);
        }
    };

    var haveStringOrValue = function () {
        return function(value) {
            return (value ? value : 'NÃO');
        };
    };

    var haveString = function () {
        return function(value) {
            return (value.lenght > 0 ? 'SIM' : 'NÃO');
        };
    };

    angular.module('guiadr')
    .filter('toShortDateBrazil', toShortDateBrazil)
    .filter('toStrFromObj', toStrFromObj)
    .filter('totalObj', totalObj)
    .filter('haveStringOrValue', haveStringOrValue)
    .filter('haveString', haveString);
}());