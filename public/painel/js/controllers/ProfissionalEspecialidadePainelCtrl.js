angular.module('guiadr').controller('ProfissionalEspecialidadePainelCtrl', function (Api, $scope, $routeParams, $location, $timeout) {
    $scope.itens = [];
    $scope.nomePagina = 'Especialidades';
    $scope.registro = new Api.ProfissionalEspecialidade();
    listAllEspecialidade();
    listAll();

    function listAllEspecialidade() {
        Api.Especialidade.get(function(registros) {
            $scope.especialidades = registros.itens;
        });
    }

    function listAll() {
        Api.ProfissionalEspecialidade.get(
            { id: $routeParams.registerId },
            function (registro) {
                $scope.registro = registro;
                $scope.itens = registro.especialidade;
            }
        );
    }
    
    $scope.remove = function (registro) {
        bootbox.confirm("Tem certeza que deseja excluir o registro?", function(answer) {
            if(answer == true) {
                Api.ProfissionalEspecialidade.delete({id: registro._id}, 
                    function(res) {                        
                        var index = -1;
                        var comArr = eval($scope.itens);
                        
                        for(var i = 0; i < comArr.length; i++) {
                            if(comArr[i]._id === registro._id) {
                                index = i;
                                break;
                            }
                        }
                        
                        if(index === -1) {
                            listAll();
                        }

                        $scope.itens.splice(index, 1);
                        $scope.totalItems--;
                    }, 
                    function(erro) {
                        bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
                    }
                );
            }
        });
    };
    
    $scope.salva = function () {
        $("#botaoSalvar").text("Aguarde!");
        $scope.registro.$save()
        .then(function (res) {
            $("#botaoSalvar").text(res.mensagem).removeClass("btn-inverse").addClass(res.class);
            listAll();
            especialidadeRemove(res.id);
        }, function() {
            bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
        });
    };
    
    $scope.avanca = function () {
        $("#botaoAvanca").text("Aguarde!");
        $location.path('profissionalLocais/' + $routeParams.registerId);
    };

    $scope.volta = function () {
        $("#botaoVolta").text("Aguarde!");
        $location.path('profissional/' + $routeParams.registerId);
    };

    var especialidadeRemove = function (id) {
        var index = -1;
        var especialidades = eval($scope.especialidades);
        if(especialidades == null) return false;
        for(var i = 0; i < especialidades.length; i++) {
            if(especialidades[i]._id === id) {
                index = i;
                break;
            }
        }
        if(index === -1) {
            listAllEspecialidade();
        }
        $scope.especialidades.splice(index, 1);
    }
});